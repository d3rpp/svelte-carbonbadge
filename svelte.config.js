import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter(),
		paths: {
			// Needed because we host this on GitLab pages
			// https://davidhund.gitlab.io/svelte-carbonbadge/
			base: '/svelte-carbonbadge'
		}
	}
};

export default config;
