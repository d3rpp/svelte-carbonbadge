# Svelte Websitecarbon Badge component

Svelte component for https://www.websitecarbon.com/badge/

## Installation

Install via npm `npm install svelte-carbonbadge --save`
or via yarn `yarn add svelte-carbonbadge`

## Usage

```
    // some.svelte
    import Carbonbadge from "svelte-carbonbadge";
    //...
    <Carbonbadge />
    // Or for Dark-Mode: <Carbonbadge dark />
```

[Live Example](https://davidhund.gitlab.io/svelte-carbonbadge/)

## See also

- [react-carbonbadge](https://github.com/ivoba/react-carbonbadge/)
- [vue-carbonbadge](https://github.com/niklashaug/vue-carbonbadge)

## Component

This component is built [and packaged](https://kit.svelte.dev/docs#packaging) through [SvelteKit](https://kit.svelte.dev/)

It can be build with `npm run build` and (then) previewed with `npm run preview`. [Example at davidhund.gitlab.io/svelte-carbonbadge/](https://davidhund.gitlab.io/svelte-carbonbadge/)
